import time
import board
import busio
import adafruit_ads1x15.ads1115 as ADS
from adafruit_ads1x15.analog_in import AnalogIn

i2c = busio.I2C(board.SCL, board.SDA)
ads = ADS.ADS1115(i2c)
ads.gain = 1

interval = 2  # How long we want to wait between loops (seconds)

def get_wind_direction(val):
    if 5700 <= val <= 6100: #5900
        return "North"
    elif 15500 <= val <= 15900: #15700
        return "North-Northeast"
    elif 13500 <= val <= 15000: #14200
        return "Northeast"
    elif 23900 <= val <= 24100: #24000
        return "East-Northeast"
    elif 23700 <= val <= 23900: #23800
        return "East"
    elif 24300 <= val <= 24700: #24500
        return "East-Southeast"
    elif 21200 <= val <= 21600: #21400
        return "Southeast"
    elif 22700 <= val <= 23100: #23000
        return "South-Southeast"
    elif 18500 <= val <= 18900: #18700
        return "South"
    elif 19700 <= val <= 20100: #19900
        return "South-Southwest"
    elif 9700 <= val <= 10100: #9900
        return "Southwest"
    elif 10500 <= val <= 10900: #10700
        return "West-Southwest"
    elif 1700 <= val <= 2100: #1900
        return "West"
    elif 4700 <= val <= 5100: #4900
        return "West-Northwest"
    elif 3200 <= val <= 3600: #3400
        return "Northwest"
    elif 7800 <= val <= 8200: #8000
        return "North-Northwest"
    else:
        return "Unknown"

while True:
    time.sleep(interval)

    # Calculate wind direction based on ADC reading
    chan = AnalogIn(ads, ADS.P0)
    val = chan.value
    # print(val)
    wind_direction = get_wind_direction(val)
    print("Wind Direction:", wind_direction)
