# Wind Vane with Raspberry Pi Documentation

## Overview

The Wind Vane, also known as wind indicator used to measure the direction of the wind. The wind vane operates based on a straightforward principle, it aligns itself with the direction of the wind due to his shape.

![Wind Vane](assets/wind_vane.png)

## How wind vane Works

- It works by the wind exerting force on a vertical blade which rotates to find the position of least resistance, then aligned with the direction of the oncoming wind.
- The magnet as been placed in top of the wind vane. The base have a circuit board with 8 reed switches, each switches connected to a different resistor.
- As the magnet rotates, may close two switches at once, allowing up to 16 different positions to be indicated.

![Wind Directions](assets/wind-vane-directions.png)

## Testing Wind vane Using Multimeter
- First find out the wind vane wires from the RJ-11 connector, Which has 4 pins (or) wires where in our case the anemometer can to connected with the wind vane using RJ-11 with two pins, using multimeter in connectivity mode.we can find the anemometer pins from 4 pined wind vane's RJ11 connector.
- After finding the anemometer pins as we know anemometer has a GND and a GPIO pin, so the GND is ${\color{red}Redpin}$ common for both the wind vane and anemometer and the A0 is ${\color{green}Greenpin}$, VCC ${\color{black}Blackpin}$
- Connect the VCC and A0 pins of wind vane with probes of Multimeter in resistance mode 20 kilo-ohms (kΩ) and by refering the (Direction, Resistance, Voltage) table in data sheet check the resistance of the wind vane from 0&deg; to 337.5&deg; respectively if you get the same resistance for the degrees then your wind vane is working correctly.

## Installation

### Requirement

- Raspberry Pi with GPIO pins
- Machanical Wind vane Sensor
- Any analog to digital convertor (ADS1115)
- 10k ohms resistor
- RJ connector, Breadboard and jumper wires (if needed)
- Multimeter

### Pin Configuration

Firstly, connect ADC to raspberry Pi. Then connect the sensor to ADC following the steps below

- **VCC**: Connect to 3v or 5v pin
- **Output Pin**: Connect to A0 in the ADC(analog to digital convertor) 
- **GND Pin**: Connect to any GND pin
- **Resistor**: Connect the 10ohms resistor in between GND to A0

![Pin connection for wind vane](assets/wind-vane-pins.png)

## Wind vane Python Code

Below is the Python code to read the Wind Direction from the Wind vane:

```python
import time
import board
import busio
import adafruit_ads1x15.ads1115 as ADS
from adafruit_ads1x15.analog_in import AnalogIn

i2c = busio.I2C(board.SCL, board.SDA)
ads = ADS.ADS1115(i2c)
ads.gain = 1

interval = 2  # How long we want to wait between loops (seconds)

def get_wind_direction(val):
    if 5700 <= val <= 6100: #5900
        return "North"
    elif 15500 <= val <= 15900: #15700
        return "North-Northeast"
    elif 13500 <= val <= 15000: #14200
        return "Northeast"
    elif 23900 <= val <= 24100: #24000
        return "East-Northeast"
    elif 23700 <= val <= 23900: #23800
        return "East"
    elif 24300 <= val <= 24700: #24500
        return "East-Southeast"
    elif 21200 <= val <= 21600: #21400
        return "Southeast"
    elif 22700 <= val <= 23100: #23000
        return "South-Southeast"
    elif 18500 <= val <= 18900: #18700
        return "South"
    elif 19700 <= val <= 20100: #19900
        return "South-Southwest"
    elif 9700 <= val <= 10100: #9900
        return "Southwest"
    elif 10500 <= val <= 10900: #10700
        return "West-Southwest"
    elif 1700 <= val <= 2100: #1900
        return "West"
    elif 4700 <= val <= 5100: #4900
        return "West-Northwest"
    elif 3200 <= val <= 3600: #3400
        return "Northwest"
    elif 7800 <= val <= 8200: #8000
        return "North-Northwest"
    else:
        return "Unknown"

while True:
    time.sleep(interval)

    # Calculate wind direction based on ADC reading
    chan = AnalogIn(ads, ADS.P0)
    val = chan.value
    # print(val)
    wind_direction = get_wind_direction(val)
    print("Wind Direction:", wind_direction)
```

## Calibration

- The above given code has different ranges which is the measure of raw values from the adc we used ADS1115 it may differ in some cases, As the wind vane is the most complicated of the three sensors because we have to fix the different ranges for different degrees by the raw values we get.
- Raw values can be measured by running the below code start from 0&deg; which is noted as north in wind vane and start marking the readings from North to East then south and then west after that for NE, SE, SW, NW ranges mark a center point in wind vane between NE, SE, SW, NW and note the different in the readings the fix a range for those directions. 
- For NNE, ENE, ESE, SSE, SSW, WSW, WNW, NNW which reading ranges can be get in between N and NE, NE and E, E and SE, SE and S, S and SW, SW and W, W and NW, NW and N which should be measured by moving the vane inch by inch slowly and steadily to get the correct value differents.
```python 
import time
import board
import busio
import adafruit_ads1x15.ads1115 as ADS
from adafruit_ads1x15.analog_in import AnalogIn

i2c = busio.I2C(board.SCL, board.SDA)
ads = ADS.ADS1115(i2c)
ads.gain = 1

interval = 1  # How long we want to wait between loops (seconds)
 # Calculate wind direction based on ADC reading
    chan = AnalogIn(ads, ADS.P0)
    val = chan.value
    print(val)
```
- By using the reading ranges for 16 different directions modify the code of the wind vane given above.

![Direction](assets/directions.png)
