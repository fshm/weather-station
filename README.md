## Step 1: Download and install Raspbian onto microSD card

[Download and install Raspian](https://www.raspberrypi.com/documentation/computers/getting-started.html) or Raspbian Lite, a common OS for Pi.
Once Raspbian has been flashed to the MicroSD card, boot up the Pi and go through the initial set up (language, WiFi, updates, etc).
Ensure Wi-Fi is connected to access the Raspberry Pi remotely

## Step 2:   Configure your Raspberry Pi using `raspi-config`:


1. **Open Terminal and Run `raspi-config`:** In the terminal, type the following command and press Enter:

   ```
   sudo raspi-config
   ```
2. **Change User Password:** For security reasons, it's recommended to change the default password of the "pi" user. In the "1 System Options" menu, select "S2 Change User Password."
3. **Network Options:** You can configure Wi-Fi and network settings here. In the "2 Network Options" menu, you can set up Wi-Fi, change the hostname, or configure other network settings.
4. **Boot Options:** Configure the boot behavior of your Raspberry Pi. You can choose to boot to the desktop or the command line. In the "3 Boot Options" menu, you'll find various boot-related settings.
5. **Localization Options:** Set your locale, timezone, and keyboard layout. This is important for regional settings. In the "4 Localisation Options" menu, you can configure these settings.
6. **Interfacing Options:** Enable or disable specific hardware interfaces

   - **SSH** - This option allows you to disable or enable SSH access to your PI. Enabling this will allow you to access your Pi from a remote location. If you plan on using the Raspberry Pi on a public network, then make sure you have changed the pi user’s password.
   - **SPI** - Allows you to disable or enable the SPI (Serial Peripheral Interface)
     kernel module which is needed by PiFace. It will allow you to connect a
     four wire serial link, so you have sensors, memory, and peripherals.
   - **I2C** - Allows you to disable or enable the I2C kernel module, so you’re able to connect I2C devices.
   - **Serial** - This option allows you to disable or enable shell and kernel messages from the serial connection.
   - **1-Wire** - Turn on the 1-wire interface if you plan on using something like the DS18B20 temperature sensor or other devices that make use of the 1-wire protocol.
   - **Remote GPIO** - This option allows remote access to the GPIO pins and should be enabled for our project work
7. **Overclocking (optional):** If you're comfortable with it, you can overclock your Raspberry Pi for better performance. This is in the "7 Advanced Options" menu.
8. **Update `raspi-config`:** After making any changes, you might need to update `raspi-config`. You'll be prompted to do this when you exit the tool. Once you've made the desired changes, exit and Reboot the Pi to apply the changes.

Remember, some changes might require a reboot to take effect. Additionally, be cautious when modifying advanced settings like overclocking, as it can potentially cause stability issues.

## Step 3: Check I2C Connections

To use I2C Communication Interface for Raspberry Pi, install the following packages

```
sudo apt-get install python-smbus
sudo apt-get install i2c-tools
```

When you are done, run this command after connecting the sensor to check in which address the sensor is connected

```
i2cdetect -y 0 
```

or

```
i2cdetect -y 1
```

## Step 4: Connect Sensors

Wire and connect the sensors as given in the documentation specified in each section of sensors to the appropriate GPIO pins on the Raspberry Pi.

### Contents
File name | Sensor Description
-----|------------
[BH1750](/BH1750/README.md) | BH1750 is a light intensity sensor
[BMP280](/BMP280/README.md) | BMP280 is a temperature and Pressure sensor
[DS18B20](/DS18B20/README.md)| DS18B20 is a digital temperature sensor
[GY-ML8511](GY-ML8511/README.md)| GY-ML8511 is a UV sensor
[LTR390](LTR390/README.md)| LTR390 is an UV sensor that can measure both ambient light and UVA light
[MQ-135-Gas-Sensor](/MQ-135-Gas-Sensor/README.md)|MQ135 is Gas sensor
[Rain-Gauge](/Rain-Gauge/README.md)| Rain Gauge is used to measure Amount of rainfall
[Wind-Vane](/Wind-Vane/README.md) | Wind vane is used to determine the Direction of Wind
[anemometer](/anemometer/README.md) | Anemometer is used to determine the Speed of Wind
[Weather-Station-Kit](kit.md) | Materials in the Weather Station Kit
[LICENCE](LICENCE)| This project is licensed under the GNU General Public License v3.0


## Step 5: Clone Repository

- Clone the Weather Station project repository  using `git clone`
- Change the terminal's current directory to the project folder.

## Step 6: Python Installation and Virtual environment Setup

1. **Python Installation**: Ensure Python is installed.

2. **Virtual Environment Setup**: Install `virtualenv` with `pip install virtualenv`.

3. **Create Virtual Environment**: Use `virtualenv venv --python=python_version`.

4. **Activate Virtual Environment**: Activate using `source ./venv/bin/activate`.

5. **View Installed Packages**: Check installed packages with `pip list`.

6. **Deactivate Virtual Environment**: Deactivate with `deactivate`.

## Step 7: Install the Dependencies 

To install the required dependencies of this Project, run
```
pip install -r /path/to/requirements.txt
``` 
## Step 8: Weather-Station Startup Service

To ensure your Python script runs automatically on boot after a power outage, create a systemd service. This will allow your script to start automatically once the Raspberry Pi boots up.

1. **Create a Python script:** Save your weather station data reading code in a Python script, let's say `weather_station.py`.

2. **Create a systemd service file:** Create a service file named `weather_station.service` in the `/etc/systemd/system/` directory. You can use the following command to create and edit the file:

   ```
   sudo nano /etc/systemd/system/weather_station.service
   ```

   Then, add the following content to the file:

   ```plaintext
   [Unit]
   Description=service for fshm weather station
   After=network.target

   [Service]
   ExecStart=/usr/bin/python3 /path/to/your/weather_station.py
   WorkingDirectory=/path/to/your/script/directory
   ExecReload=/bin/kill -s HUP $MAINPID
   KillMode=mixed
   TimeoutStopSec=5
   PrivateTmp=true
   #Restart=always
   #User=pi  # Change to your user if not 'pi'

   [Install]
   WantedBy=multi-user.target
   ```

   Replace `/path/to/your/weather_station.py` with the actual path to your Python script. Make sure you provide the correct user under `User=` as well.

3. **Enable and start the service:** Use the following commands to enable and start the service:

   ```bash
   sudo systemctl enable weather_station.service
   sudo systemctl start weather_station.service
   ```

4. **Test the service:** Reboot the Raspberry Pi to ensure that the service starts automatically and runs your Python script after a power outage.

With these steps, your Python script should run automatically on boot, even after a power outage. The systemd service will take care of starting and managing your script's execution.