 # MQ135 Gas Sensor with Raspberry Pi Documentation
 
 ## overview
 The MQ135 is one of the popular gas sensor from the MQ series of sensors that are commonly used in air quality control.

 ![MQ-135-Gas-Sensor](assets/MQ-135.png)

 ### Purpose

 They are used in air quality control equipments for buildings/offices, are suitable for detecting
 of Ammonia(NH3), Sulfur(S), Nitric Oxide(NOx), alcohol, Benzene(C6H6), smoke, CO2 ,etc.

 ### Technical Specifications

 - Operating voltage: 2.5V to 5.0V
 - Power consumption: 150mA
 - Detects: NH3,Nox,CO2,Alcohol, Benzene, Smoke
 - Typical operating voltage: 5V
 - Digital output: 0V to 5V @5V Vcc
 - Analog output: 0-5V @5V Vcc

 ## Installation
 
 ### Requirements

 - Raspberry Pi with GPIO pins
 - MQ135 Gas Sensor
 - ADS1115(ADC)
 - Breadboard and jumper wires
 
 ### Wiring 

 Firstly, connect ADS1115 to Raspberry Pi .Then connect the sensor to ADS1115 following the steps below:
 - Vcc Pin: Connect this to 3.3v power pin 
 - GND Pin: Connect this to the ground pin
 - AO Pin: Connect this to the AO pin of ADC(ADS1115) 
 
 ![connection Sensor to Rpi](assets/MQ-135-pins.png)
 
 ### MQ135 Gas sensor Python Code

 Below is the Python code to read the value from the Gas Sensor:
 ```python
 import time
 import Adafruit_ADS1x15

 # Create an ADC object for the ADS1115.
 adc = Adafruit_ADS1x15.ADS1115()

 # adjust the gain to a suitable value for your sensor.
 # The default is 2/3, which means input range of +/-6.144V.
 # Other options include 1, 2, 4, 8, and 16.
 GAIN = 2/3

 # Define the MQ135 sensor characteristics.
 MQ135_RL = 10.0   # Load resistance in kilo-ohms.
 MQ135_R0 = 76.63  # Sensor resistance at 100 ppm of NH3 gas in kilo-ohms.
 MQ135_PIN = 0     # ADS1115 channel to which the MQ135 is connected.

 def read_mq135_gas_concentration():
    # Read the raw analog value from the ADC.
    raw_value = adc.read_adc(MQ135_PIN, gain=GAIN)

    # Calculate the voltage based on the raw value and ADC gain.
    voltage = (raw_value / 32767.0) * 6.144

    # Calculate the resistance of the MQ135 sensor using voltage divider formula.
    mq135_resistance = (MQ135_RL * voltage) / (6.144 - voltage)

    # Calculate the gas concentration in parts per million (ppm).
    mq135_ppm = 100 * (mq135_resistance / MQ135_R0) ** 2.769

    return mq135_ppm

def main():
    try:
        while True:
            ppm = read_mq135_gas_concentration()
            print(f"MQ135 Gas Concentration: {ppm:.2f} ppm")
            time.sleep(1)

    except KeyboardInterrupt:
        print("Exiting...")
    finally:
        adc.stop_adc()

if __name__ == "__main__":
    main()

 ``` 
 ## Note
 - The threshold value can be set by using the on-board potentiometer. Refer [Datasheet](https://www.olimex.com/Products/Components/Sensors/Gas/SNS-MQ135/resources/SNS-MQ135.pdf)
 - Sensor requires some pre-heating before it could actually give accurate results

