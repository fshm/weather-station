import time
import Adafruit_ADS1x15

# Create an ADC object for the ADS1115.
adc = Adafruit_ADS1x15.ADS1115()

# adjust the gain to a suitable value for your sensor.
# The default is 2/3, which means input range of +/-6.144V.
# Other options include 1, 2, 4, 8, and 16.
GAIN = 2/3

# Define the MQ135 sensor characteristics.
MQ135_RL = 10.0   # Load resistance in kilo-ohms.
MQ135_R0 = 76.63  # Sensor resistance at 100 ppm of NH3 gas in kilo-ohms.
MQ135_PIN = 0     # ADS1115 channel to which the MQ135 is connected.

def read_mq135_gas_concentration():
    # Read the raw analog value from the ADC.
    raw_value = adc.read_adc(MQ135_PIN, gain=GAIN)

    # Calculate the voltage based on the raw value and ADC gain.
    voltage = (raw_value / 32767.0) * 6.144

    # Calculate the resistance of the MQ135 sensor using voltage divider formula.
    mq135_resistance = (MQ135_RL * voltage) / (6.144 - voltage)

    # Calculate the gas concentration in parts per million (ppm).
    mq135_ppm = 100 * (mq135_resistance / MQ135_R0) ** 2.769

    return mq135_ppm

def main():
    try:
        while True:
            ppm = read_mq135_gas_concentration()
            print(f"MQ135 Gas Concentration: {ppm:.2f} ppm")
            time.sleep(1)

    except KeyboardInterrupt:
        print("Exiting...")
    finally:
        adc.stop_adc()

if __name__ == "__main__":
    main()
