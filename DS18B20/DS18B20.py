import time
from w1thermsensor import W1ThermSensor

ds18b20 = W1ThermSensor()

interval = 5 #How long we want to wait between loops (seconds)

while True:

    time.sleep(interval)

    #Pull Temperature from DS18B20
    temperature = ds18b20.get_temperature()

    #Print the results
    print('Temperature:' ,temperature)

