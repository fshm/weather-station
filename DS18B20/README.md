# DS18B20 Temperature Sensor with Raspberry Pi Documentation
 
## overview

The DS18B20 is a digital temperature sensor that communicates with the [“One-Wire”](https://docs.arduino.cc/learn/communication/one-wire) communication protocol, a proprietary serial communication protocol that uses only one wire to transmit the temperature readings to the microcontroller.

![DS18B20 Sensor](assets/DS18B20.jpg)

### Technical Specifications

 - -55°C to 125°C range
 - Accuracy: ±0.5°C
 - 3.0V to 5.0V operating voltage
 - 750 ms sampling
 - 0.5°C (9 bit); 0.25°C (10 bit); 0.125°C (11 bit); 0.0625°C (12 bit) resolution
 - 64 bit unique address
 - One-Wire communication protocol

## Installation
 
### Requirements

 - Raspberry Pi with GPIO pins
 - DS18B20 Temperature Sensor
 - 4.7K Ohm or 10K Ohm resistor
 - Breadboard and jumper wires
 
## Connection

### Enable 1-wire interface on Raspberry Pi

We’ll need to enable the One-Wire interface before the Pi can receive data from the sensor. Then follow these steps to enable the One-Wire interface:

1. Open the Terminal on Raspberry Pi.
2. Run the following command to edit the config file:
```bash
sudo nano /boot/config.txt
```
3. Add the following line to the end of the file:
```bash
dtoverlay=w1-gpio
```
4. Save the file (ctrl + S) and exit (Ctrl + X).
5. Reboot the Raspberry Pi to apply changes.
```bash
reboot
```
### Pin Configuration 

 - **VCC Pin**: Connect in 3.3v pin
 - **DQ Pin**: Connect in any GPIO (eg: GPIO4)
 - **GND Pin**: Connect in ground pin
 - **resistor**: Connect between VCC and DQ
 
 ![Sensor to Rpi pin connection](assets/ds18e20-pin.png)
 
### DS18B20 Temperature sensor Python Code

Below is the Python code to read the value from the UV Sensor:
 
 ```python
 import time
from w1thermsensor import W1ThermSensor

ds18b20 = W1ThermSensor()

interval = 5 #How long we want to wait between loops (seconds)

while True:

    time.sleep(interval)

    #Pull Temperature from DS18B20
    temperature = ds18b20.get_temperature()

    #Print the results
    print('Temperature:' , temperature)

 ``` 

## Note

 This documentation is meant to provide a general overview of the DS18B20 temperature sensor. For specific implementation details and advanced usage, refer to the manufacturer's documentation and resources. Refer [Datasheet](https://www.circuitbasics.com/wp-content/uploads/2016/03/DS18B20-Datasheet.pdf)

