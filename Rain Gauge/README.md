# Rain gauge with Raspberry Pi Documentation

## Overview

This rain gauge is basically a self-emptying tipping bucket. It collecting and measuring the amount of water accumulated from rain or other forms of precipitation(rainfall). The data collected by rain gauges help in understanding weather patterns, assessing droughts and floods, and studying long-term climate changes.

![Rain Gauge](assets/20230803_123814_rain_gauge.jpg)

## How Rain Gauge Works

- The rain gauge consists of a funnel-shaped collection area that collects rainwater and pours it into a measuring chamber.
- The measuring chamber contains a container divided into 2 oscillating buckets. When one bukcet fills with water, it tips over, the watre will drain out from the base, while the other compartment takes its place to collect more rainwater.
- Each buckets carry **0.2794 mm** of water to tip the bucket.
- Each time the bucket tips, an mechanical sensor detects the motion, and a count is registered. Each tip represents a predefined volume of water, often 0.2 mm or 0.01 inches of rainfall.

![Tipping Bucket rain gauge function](assets/Rain%20gauge.png)

## Connections

Connect the Rain gauge to the Raspberry Pi as follows:

- **Rain gauge Output Pin**: Connect this to any GPIO input pin on the Raspberry Pi (e.g., GPIO17).
- **Rain gauge GND Pin**: Connect this to any GND (Ground) pin on the Raspberry Pi.

## Rain Gauge Python Code

Below is the Python code to read the rainfall from the rain gauge:

```python
import RPi.GPIO as GPIO
import time

# this many mm per bucket tip
CALIBRATION = 0.2794

# which GPIO pin the gauge is connected to
PIN = 17 

# file to log rainfall data in
LOGFILE = "log.csv"

GPIO.setmode(GPIO.BCM)  
GPIO.setup(PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)

# variable to keep track of how much rain
rain = 0

# the call back function for each bucket tip
def cb(channel):
	global rain
	rain = rain + CALIBRATION

# register the call back for pin interrupts
GPIO.add_event_detect(PIN, GPIO.FALLING, callback=cb, bouncetime=300)

# open the log file
file = open(LOGFILE, "a")

# display and log results
while True:
	line = "%i, %f" % (time.time(), rain)
	print(line)
	file.write(line + "\n")
	file.flush()
	rain = 0
	time.sleep(5)

# close the log file and exit nicely
file.close()
GPIO.cleanup()
```

## Calibration

Calibrate the rain gauge by pouring a known volume of water into it, measuring the collected water, and calculating the calibration factor (CF) as follows: 
**CF = Known Volume / Collected Volume.** 
Apply the CF to future rainfall measurements: **Corrected Rainfall = Measured Rainfall * CF.**

## Note

- The calibration procedure may vary depending on the type of rain gauge and the manufacturer's guidelines. Always refer to the rain gauge's manual or specific calibration instructions provided by the manufacturer.
- Ensure to follow standard measurement practices and use high-precision measuring instruments for accurate calibration.
- Keep detailed records of the calibration process, including calibration factor, dates, and any deviations observed during verification.



