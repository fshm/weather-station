# Weather Station Kit

- Raspberry Pi 3 Model B Plus
- Raspberry pi zero  
- Thermostat
- Bread board 
- Wind Wane
- Anemometer
- Rain gauge
- Jumper Wire(strips)
    1. Male to Male
    2. Male to Female
    3. Female to Female 
- RJ11 Connector X 3
- Weather station mount + clamp
- Card Reader X 1
- HDMI to Mini HDMI OTG Cable X 1

## Sensors

- BMP280
- DS18B20
- LTR390
- BH1750
- GYML8511
- MQ135
