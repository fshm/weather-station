# Anemometer with Raspberry Pi Documentation

## Overview

The anemometer is used as a rotation-based sensor that generates pulses proportional to the wind speed. We will use the Raspberry Pi's GPIO to detect these pulses and calculate the wind speed.

![anemometer](assets/anemometer.png)

## How to works

 - The wind catches the three cups and drives them round, spinning the central section.
 - The small magnet placed in underside of the rotating cups. Inside the bottom base the reed switch is connected with the Rj wires.
 - The magnet will rotate in a tight circle above the reed switch. So for every complete rotation, there will be two moments when the switch is closed.
 - If we can detect the number of rotations in a given time period, we can calculate the speed at which the arms are spinning. As some energy is lost in the pushing of the cups, an anemometer often under-reports the wind speed. 

## Requirements

- Raspberry Pi (any model with GPIO pins)
- Anemometer with pulse output (e.g., rotation-based wind speed sensor)
- Breadboard and jumper wires
- RPi.GPIO library

## Connections
Connect the anemometer to the Raspberry Pi as follows:

- **Anemometer Output Pin**: Connect this to any GPIO input pin on the Raspberry Pi (e.g., GPIO17).
- **Anemometer GND Pin**: Connect this to any GND (Ground) pin on the Raspberry Pi.

## Anemometer Python Code

Below is the Python code to read the wind speed from the anemometer:

```python
import RPi.GPIO as GPIO
import time

# Set the GPIO mode and pin number
GPIO.setmode(GPIO.BCM)
anemometer_pin = 17

# Variables to track wind speed and previous reading
wind_speed_m_per_s = 0.0
previous_reading = 0

def calculate_wind_speed(new_reading):
    global wind_speed_m_per_s, previous_reading
    delta_time = time.time() - previous_reading
    if delta_time > 0:
        wind_speed_m_per_s = 2.4 / delta_time  # 2.4 is a calibration constant
    previous_reading = new_reading

# Setup the GPIO pin
GPIO.setup(anemometer_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)

try:
    while True:
        # Detect the rising edge (change from LOW to HIGH)
        GPIO.wait_for_edge(anemometer_pin, GPIO.RISING)

        # Record the current time as the new reading
        current_time = time.time()

        # Calculate and print the wind speed
        calculate_wind_speed(current_time)
        print(f"Wind Speed: {wind_speed_m_per_s:.2f} m/s")

except KeyboardInterrupt:
    print("\nMeasurement stopped by the user.")
finally:
    GPIO.cleanup()
```

## Running the Code

1. Connect the anemometer to the Raspberry Pi following the provided connections.
2. Copy and paste the Python code into a new Python file on your Raspberry Pi.
3. Run the Python script using the command: `python script_name.py`
4. The script will continuously read the wind speed and display it in meters per second.

## Calibration

The accuracy of the wind speed calculation may vary based on the anemometer design and environmental conditions. Adjust the `2.4` calibration constant in the code to match your anemometer's calibration for accurate readings. Refer the official site for [calibiration](https://projects.raspberrypi.org/en/projects/build-your-own-weather-station/5https:/)

## Note

- This code assumes that the anemometer generates pulses with each revolution of its cups.
- The wind speed displayed is approximate and may require further calibration based on your specific anemometer's characteristics.
