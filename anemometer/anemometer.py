import RPi.GPIO as GPIO
import time

# Set the GPIO mode and pin number
GPIO.setmode(GPIO.BCM)
anemometer_pin = 17

# Variables to track wind speed and previous reading
wind_speed_m_per_s = 0.0
previous_reading = 0

def calculate_wind_speed(new_reading):
    global wind_speed_m_per_s, previous_reading
    delta_time = time.time() - previous_reading
    if delta_time > 0:
        wind_speed_m_per_s = 2.4 / delta_time  # 2.4 is a calibration constant
    previous_reading = new_reading

# Setup the GPIO pin
GPIO.setup(anemometer_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)

try:
    while True:
        # Detect the rising edge (change from LOW to HIGH)
        GPIO.wait_for_edge(anemometer_pin, GPIO.RISING)

        # Record the current time as the new reading
        current_time = time.time()
        
        # Calculate and print the wind speed
        calculate_wind_speed(current_time)
        print(f"Wind Speed: {wind_speed_m_per_s:.2f} m/s")
       
except KeyboardInterrupt:
    print("\nMeasurement stopped by the user.")
finally:
    GPIO.cleanup()
