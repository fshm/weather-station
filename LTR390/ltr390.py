
import time
import board
import adafruit_ltr390

i2c = board.I2C()  # uses SCL and SDA
ltr = adafruit_ltr390.LTR390(i2c)

while True:
    print("UV:", ltr.uvs) 
    print("UVI:", ltr.uvi) 
    print("Ambient Light:", ltr.light)
    #print("Lux:", ltr.lux) #The sensor BH1750 gives accurate lux than ltr390
    print("---------------------------")
    time.sleep(1.0)
