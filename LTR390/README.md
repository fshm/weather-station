# LTR390 Ambient Light & UV Sensor

## 1. Sensor Overview

### Overview:

The LTR390 is an UV sensor that can measure both ambient light and UVA light.  This can be used to measure the amount of UV radiation in the environment over time. This information can be used to track changes in UV radiation levels and to assess the impact of climate change.

### Key Features:

- Environmental monitoring:
- I2C interface
- Built-in temperature compensation
- Programmable interrupt
- Wide dynamic range
- Accuracy and reliability
- Programmable sleep mode
- Internal ADC

![20230809_193503_ltr390.jpg](assets/20230809_193503_ltr390.jpg)

## 2. Installation:

### Mounting:

The LTR390 sensor should be mounted in a location where it is not exposed to direct sunlight. This is because the sensor is sensitive to UV radiation, and direct sunlight can cause the sensor to overheat and damage, The sensor should also be mounted in a location where it is not exposed to artificial UV radiation, such as from fluorescent or LED lights.

### Connections:

1. Power: Connect the VIN pin on the sensor board to the VCC of Raspberry Pi
2. Ground: Connect the GND pin on the sensor board to the GND of Raspberry Pi
3. I2C: Connect the SCL pin on the sensor board to the SCL of Raspberry Pi
4. SDA: Connect the SDA pin on the sensor board to the SDA of Raspberry Pi


![20230809_193515_wiring_LTR_390.png](assets/20230809_193515_wiring_LTR_390.png)

### Requirements

- Raspberry Pi (any model)
- LTR390
- Breadboard and jumper wires
- adafruit_ltr390 library

#### LTR390 Python Code:

```python
import time
import board
import adafruit_ltr390
#pip install adafruit_ltr390 - to import the library to use the sensor

i2c = board.I2C()  # uses SCL and SDA
ltr = adafruit_ltr390.LTR390(i2c)
#The device I2C address defaults to 0x53

while True:
    print("UV:", ltr.uvs) 
    print("UVI:", ltr.uvi) 
    print("Ambient Light:", ltr.light)
    #print("Lux:", ltr.lux) #The sensor BH1750 gives accurate lux than ltr390
    print("---------------------------")
    time.sleep(1.0)

```

### Inference of the Output

1. Ultraviolet Index(UVI)

(UVI) is a standardized measurement that indicates the strength of ultraviolet (UV) radiation from the sun at a particular location and time. The UVI scale typically ranges from 0 to 11 or higher, which represents different levels of risk to human health:

- Low (0-2): Minimal risk of harm from unprotected sun exposure.
- Moderate (3-5): Moderate risk of harm. Protection, such as sunscreen, is recommended.
- High (6-7): High risk of harm. Protection and avoidance of sun during peak hours are advised.
- Very High (8-10): Very high risk of harm. Strict protection measures are recommended.
- Extreme (11+): Extreme risk of harm. Avoid outdoor activities during peak hours and take extensive protective measures.

2. Lux

- Lux is a unit of measurement for illuminance, which is the amount of luminous flux (light energy) per unit area. In simple terms, it measures how much light is falling on a surface.

3. Ambient Light

- Ambient light refers to the natural or artificial light present in a given environment. It's the general illumination that surrounds us.

4. UV (Ultraviolet) Light:

- Ultraviolet (UV) light is a type of electromagnetic radiation with a shorter wavelength than visible light. It's not visible to the human eye, but it has various effects on materials and organisms.

### Datasheet for References:

[LTR 390 Datasheet (PDF)](https://datasheet.lcsc.com/lcsc/2011051106_Lite-On-LTR-390UV-01_C492374.pdf)

[Adafruit_CircuitPython_LTR390](https://github.com/adafruit/Adafruit_CircuitPython_LTR390)
