import time
import board
import busio
import adafruit_bmp280 #pip install adafruit_bmp280

# Create I2C bus
i2c = busio.I2C(board.SCL, board.SDA)

# Create BMP280 instance with the appropriate address (SDO pin setting)
bmp280 = adafruit_bmp280.Adafruit_BMP280_I2C(i2c, address=0x77)  # Change to 0x77 if SDO is pulled high

# Calibrate by adjusting the offset
offset = 1127.5  # Adjust this value based on calibration

while True:
    raw_pressure = bmp280.pressure  # Read raw pressure value
    calibrated_pressure = raw_pressure + offset
    # Print temperature and pressure readings
    print("Temperature: {:05.2f} C".format(bmp280.temperature))
    print("Pressure: {:05.2f} hPa".format(calibrated_pressure))
    time.sleep(1.0)