## Temperature and Pressure Sensor (BMP280)

## Sensor Overview:

The BMP280 sensor is a versatile sensor capable of measuring temperature, pressure, and relative altitude. It is commonly used in weather stations, environmental monitoring systems, and other applications where precise environmental data is required.

![bmp280_sensor](assets/20230831_162622_bmp280_image.jpg)

## Features:

1. **Power Supply**: Typically 1.8V to 3.6V
2. **Current Consumption**: About 0.1 mA in standard mode (lowest power consumption)
3. **Measuring Range**:

   - Temperature: -40°C to +85°C
   - Pressure: 300 to 1100 hPa (hPa - hectopascals)
4. **Accuracy:**

   - Temperature: ±1.0°C (typical)
   - Pressure: ±1.0 hPa (typical)
5. **Communication:** I2C and SPI
6. **Calibration:** Factory-calibrated for temperature and pressure compensation

## Installation

### Requirements

- Raspberry Pi with GPIO pins
- BMP280 Sensor
- Breadboard and jumper wires
- adafruit_bmp280 library

### Wiring and Connection:

The BMP280 sensor requires four connections to the Raspberry Pi:

- SDO Pin: Connect in 3.3V pin
- GND Pin: Connect in Ground pin
- SCL Pin: Connect in SCL pin(GPIO 3)
- SDA Pin: Connect in SDA pin(GPIO 2)


![pin_diagram](assets/20230831_162524_bmp280.png)

## BMP280 Python Code

Below is the Python code to read the value from the sensor:

```python
import time
import board
import busio
import adafruit_bmp280 #pip install adafruit_bmp280

# Create I2C bus
i2c = busio.I2C(board.SCL, board.SDA)

# Create BMP280 instance with the appropriate address (SDO pin setting)
bmp280 = adafruit_bmp280.Adafruit_BMP280_I2C(i2c, address=0x77)  # Change to 0x77 if SDO is pulled high

# Calibrate by adjusting the offset
offset = 1127.5  # Adjust this value based on calibration

while True:
    raw_pressure = bmp280.pressure  # Read raw pressure value
    calibrated_pressure = raw_pressure + offset
    # Print temperature and pressure readings
    print("Temperature: {:05.2f} C".format(bmp280.temperature))
    print("Pressure: {:05.2f} hPa".format(calibrated_pressure))
    time.sleep(1.0)
```

## Note

The Sensor should be adjusted Mannualy by calculated Pressure from the nearest Geo-station and should set the offset value to the sensor code.
Refer for official [Documentation](https://cdn-shop.adafruit.com/datasheets/BST-BMP280-DS001-11.pdf)
