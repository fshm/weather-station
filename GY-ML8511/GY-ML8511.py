import time
import board
import busio
import adafruit_ads1x15.ads1115 as ADS
from adafruit_ads1x15.analog_in import AnalogIn

i2c = busio.I2C(board.SCL, board.SDA)

ads = ADS.ADS1115(i2c)
chan = AnalogIn(ads, ADS.P0)  # Connect ML8511 to A0 on ADS1115

def convert_voltage_to_uv(voltage):
    uv_intensity = (voltage - 0.99) * 104.167  # Conversion formula for ML8511
    return uv_intensity

def calculate_uvi(uv_intensity):
    # UVI calculation based on UV intensity
    # This formula is a simplified approximation, not an official calibration
    uvi = uv_intensity / 25.0  # Adjust the scaling factor as needed
    return uvi

while True:
    uv_voltage = chan.voltage
    uv_intensity = convert_voltage_to_uv(uv_voltage)
    uvi = calculate_uvi(uv_intensity)
    
    print("UV Voltage: {:.2f}V | UV Intensity: {:.2f} uW/cm^2 | UVI: {:.2f}".format(uv_voltage, uv_intensity, uvi))
    time.sleep(1)

