 # GY-ML8511 UV Sensor with Raspberry Pi Documentation
 
 ## overview

The GY-ML8511 is a UV sensor module that can detect the intensity of [ultraviolet (UV) radiation](https://en.wikipedia.org/wiki/Ultraviolet), commonly used in applications like UV index monitoring and exposure measurement. The GY-ML8511 sensor uses a photodiode to detect UV radiation.

 ![GY-ML8511 UV Sensor](assets/GY-ML8511.png)

 ### Features

 - Photodiode sensitive to UV-A and UV-B
 - Embedded operational amplifier
 - Analog voltage output
 - Low supply current (300A typ.) and low standby current (0.1A typ.)
 - Small and thin surface mount package (4.0mm x 3.7mm x 0.73mm, 12-pin ceramic QFN)

 ### Technical Specifications

 - Power Supply: 3.3V - 5V DC
 - Operating Temperature: -20 degree Celsius to +85 degree Celsius
 - Output Voltage Range: 0V to 1V (corresponding to 0 to 15 mW/cm^2)
 - UV Sensitivity Range: 240nm - 370nm
 - Dimensions: Approximately 25mm x 12mm

 ## Installation
 
 ### Requirements

 - Raspberry Pi with GPIO pins
 - GY-ML8511 UV Sensor
 - Any Analog to digital convertor (eg: ADS1115)
 - Breadboard and jumper wires
 
 ### Pin Configuration

 Firstly, connect ADS1115 to Raspberry Pi .Then connect the sensor to ADS1115 following the steps below:

 - **3v3 Pin**: Connect in 3.3v pin
 - **GND Pin**: Connect in ground pin
 - **OUT Pin**: Connect in A0 pin of ADC(ADS1115) 
 
 ![Sensor to Rpi pin connection](assets/GY-ML8511-pin.png)
 
 ### GY-ML8511 UV sensor Python Code

 Below is the Python code to read the value from the UV Sensor:
 
 ```python
import time
import board
import busio
import adafruit_ads1x15.ads1115 as ADS
from adafruit_ads1x15.analog_in import AnalogIn

i2c = busio.I2C(board.SCL, board.SDA)

ads = ADS.ADS1115(i2c)
chan = AnalogIn(ads, ADS.P0)  # Connect ML8511 to A0 on ADS1115

def convert_voltage_to_uv(voltage):
    uv_intensity = (voltage - 0.99) * 104.167  # Conversion formula for ML8511
    return uv_intensity

def calculate_uvi(uv_intensity):
    # UVI calculation based on UV intensity
    # This formula is a simplified approximation, not an official calibration
    uvi = uv_intensity / 25.0  # Adjust the scaling factor as needed
    return uvi

while True:
    uv_voltage = chan.voltage
    uv_intensity = convert_voltage_to_uv(uv_voltage)
    uvi = calculate_uvi(uv_intensity)
    
    print("UV Voltage: {:.2f}V | UV Intensity: {:.2f} uW/cm^2 | UVI: {:.2f}".format(uv_voltage, uv_intensity, uvi))
    time.sleep(1)
 ``` 
 ## Note

 This documentation is meant to provide a general overview of the GY-ML8511 UV sensor. For specific implementation details and advanced usage, refer to the manufacturer's documentation and resources. Refer [Datasheet](https://cdn.sparkfun.com/datasheets/Sensors/LightImaging/ML8511_3-8-13.pdfl)

